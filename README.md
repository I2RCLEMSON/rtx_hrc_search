# rtx_hrc_search (python)

## Summary
This package provides the simulation program for one-human-multi-robot collaborative search. 

This package has 4 variants: `hrc09`, `hrc09_s_shape`, `hrc09_gamma_shape`, and `hrc00`. See the different branches of this repo.

According to the teamwork design in our paper (Section 4.3), the robots can use either multi-visit strategy or single-visit strategy. 
The multi-visit strategy requires the probability of correct detection (P(s=1|o=1) or P(s=0|o=0)) be greater than 0.9 before the robots can request human service.  The single-visit strategy requires the probability of correct detection to be greater than 0.0 only.

The variants use the multi-visit strategy: `hrc09`, `hrc09_s_shape`, and `hrc09_gamma_shape`.
The variant uses the single-visit strategy: `hrc00`.
 

There are 3 types of probability weighting functions: inverted-S-shape, S-shape, and Gamma-shape. 
The variants use inverted-S w-function: `hrc00`, `hrc09`.
The variant uses S w-function: `hrc09_s_shape`,.
The variant uses Gamma w-function: `hrc09_gamma_shape`

There is a short pipeline for running the programs:
`task_region_spot.py` ⇒ `main.py`⇒ `read2means.py`

You only need to run `task_region_spot.py` **once** to specify the search domain. 
After that is done, you can run `main.py` directly every time with the pre-specified domain.
`read2means.py` is the post-process program to compute the means and variances from the data simulated in `main.py`.

The remaining programs are the supporting functions for `main.py`. They are `dijkstra.py` and `optimal_order.py`. 

## Pipeline

### `task_region_spot.py`

In this script, we define the task for each robot in the human-robot team. This script needs to run only once to define the initial prior probability of target PRESENCE in the cells in each search region.

To run this script, open this package as a project in PyCharm.
Open the file `task_region_spot.py`.
 After configuring the python interpreter (we used python 3.5, other python 3.* may also work), run the script.

You can change the settings of the domain by modifying the values of `num_region`, `num_row`, and `num_col` in lines 73--77 as follows
```python
# Let's assume there are 20 robots in the human-robot team. There are 15 independent task regions.
num_regions = 20
# Let's define the size of each region.
num_row = 30
num_col = 30
```
Running this script automatically save the domain information in the directory `./data/` for later use. 

### `main.py` 
This is the main script for the simulation. 
It defines the following classes: `class Robot`, `class Agent`, `class Human`, and `class Environment`. 
It has one main function `main()`

Let us explain them one by one.

`class Robot` contains the features of a robot in the human-multi-robot team. It has functions for doing detection, planning, transition, etc.  The `dijkstra.py` is called in this class. The most important feature is the detection capability of one robot. It is specified in lines 27--29:
```python
 # Create a sensor model for the robot detection.
 self.p_o1_s1 = 0.9  # Probability of true positive.
 self.p_o1_s0 = 0.4  # Probability of false positive.
```
`class Agent` contains the features of robot ordering. It has functions for the optimal ordering based on RTx, based on EV, and based on no-human (Assume no human in the team). The `optimal_order.py` is called in this class.

`class Human` contains the features of the human. The most important feature is the human detection cost `hm_tsk_dur`.  This cost quantifies the time duration of one human detection.
It value can be specified in line 225 as follows.
```python
# Let us first determine how many times slower human is.
 self.hm_tsk_dur = 20  # human task duration is several times slower than that of the robot detection.
```
 `class Environment` contains the features of the environment. It has the functions for domain information and the observation realization. The most important feature needing specification is the object loss cost `cost_tar`. This cost can be specified in line 263 as follows.
 ```python
 # Initialize the cost of losing one target
 self.cost_tar = -100
 ```
 
In `main()`, you can choose with decision-making model to be used in the simulation in line 302 as 
```python
 # Set the decision-making model here.
 decision_model = 'RTx'  # 'No human', 'EV', or 'RTx'
```
You can also specify the mandatory time-limit for the task in line 316 as
```python
 # We define the time limit.
 total_time_step = 18000
```
Finally, at the end of this script, you must choose **where** to save the simulation results! It is done in line 546 as
```python
 # Define a file name to save the results.
 # Delete any existing result file
 filename = 'results/condition65_RTx.txt'
```
Furthermore, you can the number of repetition of the simulation. It is specified in line 552 which looks like this
```python
# Run the experiment for 20 times and store the data.
 for i in range(19):
```
I am sorry you have to modify the specifications over the script. This is the experimental program, hence I did not provide an optimal interface for users. But it did its work. Once you run this simulation several times---I have run this stuff hundreds of times---you will be very familiar about where to make changes.

After the setting, running the script is quite simple. Open this package as a project in PyCharm.
Open the file `main.py`. 
After configuring the python interpreter (we used python 3.5, other python 3.* may also work), run the script.

The results are saved to `./results/`

### `optimal_order.py`
Here is an hidden functionality of `optimal_order.py`. In this script you can choose to use "optimal ordering", "no ordering", and "reversed optimal ordering". 
You do this setting by manually changing some lines in this script. 
Specifically, you need to find lines 162--165 as
```python
 ## ----------------------------------------------------------
 ## The following two lines are just for providing baseline in the experiments. Normally they should be commented.
 # self.robot_list_wait.reverse()  # The waiting line is in the reverse order with respect to the optimal ordering.
 # random.shuffle(self.robot_list_wait)  # The waiting line is randomly shuffled.
```
When you comment out both lines, the ordering is optimal.
If you want reversed optimal ordering, you need to comment only the second line as follows
```python
 ## ----------------------------------------------------------
 ## The following two lines are just for providing baseline in the experiments. Normally they should be commented.
 self.robot_list_wait.reverse()  # The waiting line is in the reverse order with respect to the optimal ordering.
 # random.shuffle(self.robot_list_wait)  # The waiting line is randomly shuffled.
```
If you want to no ordering, you need to comment only the first line as follows.
```python
 ## ----------------------------------------------------------
 ## The following two lines are just for providing baseline in the experiments. Normally they should be commented.
 # self.robot_list_wait.reverse()  # The waiting line is in the reverse order with respect to the optimal ordering.
 random.shuffle(self.robot_list_wait)  # The waiting line is randomly shuffled.
```
I am sorry again for making this change not so convenient. I was rushing to do the experiment and writing the paper. I know you can forgive me. 

The outputs of `optimal_order.py` are a sequence of ordered robots. This sequence is represented as the following form, for example
```
[(1, 0.9001370116252254), (10, 0.910898564413953), ...]
```
Each tuple represents (Robot_id, Detection_confidence) where the Detection_confidence is the posterior probability of a detection being correct. 

### `read2means.py`
This the post-process program for the simulation data. It computes the means and variances of different metrics. Its input is the filename of a data file. Its outputs are printed on the screen. 
You can specify the name of a data file in line 6. It looks like this.
```python
# Change the filename here.  
filename = 'condition75_RTx_opt.txt'
```
Just change that name to the data file you want to analyze. 


## Folder structure
`./data/` stores the information of the search domain. 
`./data_1161` stores previously generated domain information which has 1161 targets. If you want to use this domain specification, simply copy the files in the folder to `./data/`.
`./data_3678` stores another previously generated domain.
`./results/` stores the simulation results of running `main.py`.
`./venv/` is a virtual environment generated by PyCharm. It is not a necessary part of the main functionality in this package. 





